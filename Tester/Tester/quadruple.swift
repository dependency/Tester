//
//  quadruple.swift
//  Tester
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import Foundation
import TesterCore

public func quadruple(int: Int) -> Int {
    return double(double(int))
}